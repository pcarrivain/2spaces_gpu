/*!
  \file variables.h is distributed under MIT licence.
  \brief Header file (variables) for the 2-spaces algorithm kernel defined in 'kernels.c'.
  \author Pascal Carrivain
  \date 07 October 2020
*/
#ifndef VARIABLES_H
#define VARIABLES_H

#define MEM_SIZE (2048)
#define MAX_SOURCE_SIZE (0x100000)

double rho,T,d0,TInitialPC,T0=.0;
int space,seed,retour,A,nA,K;
int voisins;/*!< number of neighbors */
int possibles;
FILE *fOut;
char fname[1500],lattice[4],path_to[500],gpu[200],*GPU=new char[200];
int N=131072;/*!< polymer size */
int nN=17;/*!< log2 polymer size */
int L=64,nL=6;/*!< lattice edge and log2 */
int every=0;
char spq[100],*spq_tok;
double cum_MCS,pas,MCS;/*!< number of Monte-Carlo steps */

std::size_t log_size;
std::size_t source_size;
std::size_t *global_work_size=new size_t[1]();
std::size_t *local_work_size=new size_t[1]();

// OpenCL variables declaration
cl_event kernel_completion;
cl_context context=NULL;
cl_program program=NULL;
cl_kernel kernels[3];
const char fileName[]="kernels.c";
cl_ulong time_start,time_end;

int n_platforms=10,n_devices=10;
cl_int selection_platform=-1,selection_device=-1,ret;
cl_platform_id *platforms=new cl_platform_id[n_platforms];
cl_device_id *devices=new cl_device_id[n_devices];

cl_int *voisin_xyz;

cl_int *x_cum;/*!< unwrap x */
cl_int *y_cum;/*!< unwrap y */
cl_int *z_cum;/*!< unwrap z */

int *mapping;
cl_int *l_voisins;

cl_int *occupation;
cl_int *i_to_site;
int *i_to_bond1;
int *i_to_bond2;

cl_int *v_possibles;
cl_int *w_possibles;
cl_int *l_possibles;
int *voisin_voisin;

cl_ulong *seeds=new cl_ulong[2]();/*!< seeds */
cl_ulong *seeds0=new cl_ulong[1]();/*!< first half-space kernel */
cl_ulong *seeds1=new cl_ulong[1]();/*!< second half-space kernel */

#endif
