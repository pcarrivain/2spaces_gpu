#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import os
import time
import sys,getopt
import numpy as np
import data_analysis as da

from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.patches as patches

def main(argv):
    # default parameters
    rho=0.062500
    L=128
    N=131072
    try:
        opts,args=getopt.getopt(argv,"hN:L:r:")
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print('plot.py -N <polymer size>')
            print('        -L <lattice edge>')
            print('        -r <rho>')
            print('python3.7 plot.py -r 0.250000 -L 16 -N 1024')
            print('python3.7 plot.py -r 0.500000 -L 64 -N 131072')
            sys.exit()
        elif opt=="-N":
            N=int(arg)
        elif opt=="-L":
            L=int(arg)
        elif opt=="-r":
            rho=float(arg)
        else:
            pass

    colors=["black","red","green","blue"]
    rainbow=matplotlib.cm.get_cmap('rainbow')

    # read conformations
    ps=np.zeros((N,3))
    for n in range(4):
        with open("data/fcc_n{0:d}_rho{1:.6f}_L{2:d}_N{3:d}_GeForce_RTX_2080_SUPER.out".format(n+1,rho,L,N),"r") as in_file:
            lines=in_file.readlines()
            # number of conformations (N sites + one entry to give the #MCS)
            C=len(lines)//(N+1)
            print(str(C)+" conformations")
            if n==0:
                Rs2=np.zeros((N,C))
                Ps=np.zeros((N,C))
            for c in range(C):
                print("conformation 2^"+str(c)+"(2^"+str(C)+")")
                for n in range(N):
                    sl=lines[c*(N+1)+1+n].split()
                    ps[n,:3]=float(sl[0]),float(sl[1]),float(sl[2])
                contact,contact_probability=da.contact_matrix(ps,np.array([N]),64,0.0,1.5,True)
                Ps[:,c]=np.add(Ps[:,c],contact_probability[:,0])
                Rs2[:,c]=np.add(Rs2[:,c],da.square_internal_distances(ps,0,N-1,True))
    #
    ji=np.arange(1,N,1)
    fig=plt.figure("Ps")
    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$|j-i|$',ylabel=r'$P(|j-i|)$',title="",xlim=(1,N),ylim=(1e-8,1.0),xscale="log",yscale="log")
    for c in range(C):
        if (c%2)==0:
            ax.plot(ji,np.multiply(1.0/np.sum(Ps[1:N,c]),Ps[1:N,c]),color=rainbow(c/C),linestyle="-",markersize=4,label="2^"+str(c))
        else:
            ax.plot(ji,np.multiply(1.0/np.sum(Ps[1:N,c]),Ps[1:N,c]),color=rainbow(c/C),linestyle="-",markersize=4)
    ax.legend(ncol=4)
    fig.savefig("Ps_L"+str(L)+"_N"+str(N)+".png",dpi=600)
    fig.clf()
    plt.close("Ps")
    #
    fig=plt.figure("Rs2")
    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$|j-i|$',ylabel=r'$\left\langle R(|j-i|)^2\right\rangle$',title="",xlim=(1,N),ylim=(1e-1,1e6),xscale="log",yscale="log")
    for c in range(C):
        if (c%2)==0:
            ax.plot(ji,np.multiply(0.25,Rs2[1:N,c]),color=rainbow(c/C),linestyle="-",markersize=4,label="2^"+str(c))
        else:
            ax.plot(ji,np.multiply(0.25,Rs2[1:N,c]),color=rainbow(c/C),linestyle="-",markersize=4)
    xpl=ji[10:100]
    ax.plot(xpl,10.0*np.power(xpl/xpl[0],2.0),color="black",linestyle="-",label=r'$\propto|j-i|^2$')
    ax.plot(xpl,10.0*np.power(xpl/xpl[0],1.2),color="black",linestyle="--",label=r'$\propto|j-i|^{1.2}$')
    ax.plot(xpl,10.0*np.power(xpl/xpl[0],1.0),color="black",linestyle="-.",label=r'$\propto|j-i|^1$')
    ax.legend(ncol=4,loc="upper left",bbox_to_anchor=(0,1.2))
    fig.savefig("Rs2_L"+str(L)+"_N"+str(N)+".png",dpi=600)
    fig.clf()
    plt.close("Rs2")

if __name__=="__main__":
    main(sys.argv[1:])
