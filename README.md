# 2spaces_gpu

Polymer of size $`N`$ is supposed to equilibrate after a time like $`N^3`$.
Therefore, it could be difficult to study the equilibrium properties of large polymers.
The **2-spaces** algorithm already improves the efficient of each Monte-Carlo by moving half of the polymer.
We can use the GPU units to take care one of the sub-move amongst the Monte-Carlo step.

Please consider reading the two research articles
[Massively Parallel Architectures and Polymer Simulation](https://www.semanticscholar.org/paper/Massively-Parallel-Architectures-and-Polymer-Ostrovsky-Smith/f79694076e40eca0fae9b35a381e43b7abfa029c)
and
[Cellular automata for polymer simulation with application to polymer melts and polymer collapse including implications for protein folding](https://www.sciencedirect.com/science/article/pii/S0167819100000818)
for details about the method.

---

# Build, run and test

You can clone the repository and then compile using the Makefile I provide.
Before the compilation you can clean the previous build:
```bash
make mrproper
```
I provide OpenCL kernel and main source code (*-h* to print the help) to run the model.
```bash
make
./executable_name -h
```
You need C++11 in order to use pseudo-random number generator
to initialize the seed for each GPU worker.
Each worker has its own [xorshift 64-bit RNG](https://en.wikipedia.org/wiki/Xorshift).
The main code includes a sanity check (connectivity and excluded volume)
of the polymer bonds (every $`2^k`$ Monte-Carlo steps).
It also saves the conformation (append to pre-existing file).
You can use [Blender module](https://gitlab.com/pcarrivain/visualisation_with_blender)
for visualisation.
Main code uses the following functions to save the system with 3D representation
of the bonds (for visualisation with Blender).
```c++
/*! @brief save the conformation details with bond representation.
  @param name name of the file to save
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param N number of occupied sites
  @param x_cum unwrap x-coordinate
  @param y_cum unwrap y-coordinate
  @param z_cum unwrap z-coordinate
*/
void save_structure(char name[],char lattice[],int N,cl_int *x_cum,cl_int *y_cum,cl_int *z_cum);
/*! @brief save for visualisation with bond representation.
  first line is the com of the bond 1, second line is bond tangent 1,
  third line is the com of the bond 2, fourth line is bond tangent 2 ...
  @param name name of the file to save
  @param wa write or append to the file 'name'
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param N number of occupied sites
  @param x_cum unwrap x-coordinate
  @param y_cum unwrap y-coordinate
  @param z_cum unwrap z-coordinate
*/
int save_for_visualisation(char name[],const char *wa,char lattice[],int N,cl_int *x_cum,cl_int *y_cum,cl_int *z_cum);
```
[Here](https://gitlab.com/pcarrivain/2spaces_gpu/-/tree/master/data),
there is output examples of the previous functions.
```bash
ifcc_n1_rho0.500000_L16_N2048_GeForce_RTX_2060_SUPER.out
blender_fcc_n1_rho0.500000_L16_N2048_GeForce_RTX_2060_SUPER.out
...
```

The list of files is:

1. [main code](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/2spaces_gpu.cpp)
2. [source file](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/functions.cpp)
3. [header file](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/functions.h)
4. [declaration file](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/variables.h)
5. [kernels](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/kernel.c)
   There is three kernels:
   1. one of them moves the half-space you pass as argument
   2. one of them moves the half-space of odd polymer units
   3. one of them moves the half-space of even polymer units
6. [documentation](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/refman.pdf)
7. [script I use to plot internal distances and contact probabilities](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/plot.py)

The polymer size as-well-as the lattice (fcc) edge has to be a power of 2.
The kernel code uses integer variables only.
The OpenCL kernel uses the bitwise operators.
Typical use of the
[main code](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/2spaces_gpu.cpp)
is:
```bash
./executable_name -L fcc -s 1 -n 17 -l 5 -m 20 -g 'Tesla P100-PCIE-16GB' -w /scratch/2spaces_gpu
```
The previous example runs a polymer of size $`2^n=131072`$ on a fcc
lattice (edge is $`2^l=64`$) for $`2^{20}`$ Monte-Carlo steps on Tesla P100-PCIE-16GB.
The seed is 1 and the code uses the */scratch/2spaces_gpu* folder to write the conformations.
Typically, I use the two following command to get the name of the hardware I want to use:
```bash
lscpu
nvidia-smi -L
```
On my machine command *nvidia-smi -L* returns:
```bash
GPU 0: GeForce GTX 1060 3GB
GPU 1: GeForce RTX 2060 SUPER
```
I can change Tesla P100-PCIE-16GB to GeForce RTX 2060 SUPER:
```bash
./executable_name -L fcc -s 1 -n 17 -l 5 -m 20 -g 'GeForce RTX 2060 SUPER' -w /scratch/2spaces_gpu
```

---

# Example

I provide
[one complete example](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/data)
with $`N=1024`$ and $`L=16`$.
I plot data after $`N^3`$ Monte-Carlo steps (corresponding to $`2^{30}`$).
The square internal distances from initial conformation
are like $`\left\langle R(|j-i|)\propto|j-i|^2`$ (the bonds point towards the z-direction).
After $`N^3`$ Monte-Carlo steps the polymer
relaxes to a Random-Walk with $`\left\langle R(|j-i|)\propto|j-i|`$ statistics.
At small scale Self-Avoiding-Walk statistics gives $`\left\langle R(|j-i|)\propto|j-i|^{6/5}`$.
I will add soon the Mean-Square-Displacement with theoritical power laws.

---

# Benchmark

I ran four simulations per GPUs (GeForce GTX 1080 and RTX 2060 SUPER).
I ran four simulations on CPU 1 (Intel(R) Xeon(R) CPU E5-2665 0 @ 2.40GHz with
2 sockets, 8 cores per socket and 2 threads per core).
I ran four simulations on CPU 2 (Intel(R) Xeon(R) W-2145 CPU @ 3.70GHz with
1 socket, 8 cores per socket and 2 threads per core).
One Monte-Carlo step corresponds to one move of one of the two half-spaces.
I measure the time spent (in seconds) in the kernel inside the C code (OpenCL profiling tool).
I report the particle update (number of polymer unit moves per second).
The log files can be find [here](https://gitlab.com/pcarrivain/2spaces_gpu/-/blob/master/benchmark).

First case is polymer size of $`N=2^{11}`$ on fcc
lattice $`L=16`$ and $`2^{17}`$ Monte-Carlo steps.

| hardware               | run 1         | run 2         |  run 3        | run 4         |
|:-----------------------|--------------:|--------------:|--------------:|--------------:|
| CPU 1                  | 7784976 | 7883051 | 7884575 | 8054806 |
| CPU 2                  | 21109151 | 20583347 | 21038509 | 21423636 |
| GeForce GTX 1080       | 148505365 | 148480283 | 146995715 | 149292485 |
| GeForce RTX 2060 SUPER | 166162286 | 190005475 | 200296153 | 197580944 |

For such polymer size we do not see significant difference in the GPU computation time.
Therefore, I ran four simulations for polymer size of $`N=2^{14}`$ on fcc
lattice $`L=32`$ and $`2^{17}`$ Monte-Carlo steps.

| hardware               | run 1         | run 2         |  run 3        | run 4         |
|:-----------------------|--------------:|--------------:|--------------:|--------------:|
| CPU 1                  | 36707878 | 39435511 | 35027370 | 36433651 |
| CPU 2                  | 65397754 | 66301050 | 65812939 | 66301593 |
| GeForce GTX 1080       | 579833303 | 618622099 | 618092704 | 593818568 |
| GeForce RTX 2060 SUPER | 1251709757 | 1172910240 | 1186393807 | 1227515633 |

Again, for such polymer size we do not see significant
difference in the GPU computation time.
Therefore, I ran four simulations for polymer size of $`N=2^{17}`$ on fcc
lattice $`L=64`$ and $`2^{17}`$ Monte-Carlo steps.

| hardware               | run 1         | run 2         |  run 3        | run 4         |
|:-----------------------|--------------:|--------------:|--------------:|--------------:|
| CPU 1                  | 88038344 | 87886722 | 88616432 | 88793855 |
| CPU 2                  | 74273319 | 74050759 | 74211956 | 74082144 |
| GeForce GTX 1080       | 694327342 | 692977909 | 695309503 | 690984340 |
| GeForce RTX 2060 SUPER | 1383254502 | 1394835855 | 1385376627 | 1385514711 |

Eventually, I ran four simulations for polymer size of $`N=2^{20}`$ on fcc
lattice $`L=128`$ and $`2^{17}`$ Monte-Carlo steps.

| hardware               | run 1         | run 2         |  run 3        | run 4         |
|:-----------------------|--------------:|--------------:|--------------:|--------------:|
| CPU 1                  | 72928368 | 73240722 | 73464245 | 73165748 |
| CPU 2                  | 67388821 | 67563424 | 66603276 | 67732748 |
| GeForce GTX 1080       | 660766828 | 659627232 | 658997743 | 659241317 |
| GeForce RTX 2060 SUPER | 1207922798 | 1212760959 | 1198496773 | 1198885035 |

I conclude from benchmark that RTX 2060 SUPER is twice faster than GTX 1080
and at least 10 times faster than CPU (with OpenCL implementation).