CXX=g++
SRC_DIR=$(PWD)
CXXFLAGS=-Wextra -Wall -pedantic -g -std=c++0x -O3 -s
# CXXFLAGS=-I/usr/include -Wextra -Wall -pedantic -g -std=c++0x -O3 -s
# CXXFLAGS=-I/usr/include -Wextra -Wall -pedantic -g -O3 -s -mtune=native
GPU_LDFLAGS=-lOpenCL
LDFLAGS=
# GPU_LDFLAGS=-L/usr/lib -lOpenCL
# LDFLAGS=-L/usr/lib
COMMON_SOURCES=${SRC_DIR}/functions.cpp
COMMON_OBJECTS=$(COMMON_SOURCES:.cpp=.o)
SOURCES0=${SRC_DIR}/2spaces_gpu.cpp
OBJECTS0=$(SOURCES0:.cpp=.o)
EXECUTABLE0=$(SOURCES0:.cpp=)

.PHONY: all

all: $(EXECUTABLE0)

$(EXECUTABLE0): $(COMMON_OBJECTS) $(OBJECTS0)
	$(CXX) -o $@ $^ $(GPU_LDFLAGS)

%.o: %.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

mrproper:
	rm -f *~ *.o $(EXECUTABLE0)
