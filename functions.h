/*!
  \file functions.h is distributed under MIT licence.
  \brief Header file for the 2-spaces algorithm kernel defined in 'kernels.c'.
  \author Pascal Carrivain
  \date 07 October 2020
*/
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>
#include <fstream>
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <random>
#include <getopt.h>
#include <time.h>
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl_platform.h>
#include <CL/cl.h>
// #include <CL/cl.hpp>
// #include <CL/opencl.h>

#define entier64 18446744073709551616.

/*! @brief replace 'a' with 'b' in 'c'
 */
void replace_char_a_with_b(char c[],char *&new_c,const char *a,const char *b,int n);

/*! @brief check OpenCL error
 */
int CL_CHECK(cl_int retour);

/*! @brief periodic boundary condition for simple cubic lattice
 */
int CP_cs(int a,int b,int c);

/*! @brief periodic boundary condition for centered cubic lattice
 */
int CP_cc(int a,int b,int c);

/*! @brief periodic boundary condition for fcc lattice
 */
int CP_fcc(int a,int b,int c);

/*! @brief This function checks if N and A are power of 2 and store the power in the passed by reference arguments 'nN' and 'nA'.
*/
void check_N_and_A_power_of_2(const char *lattice,int L,int N,int &A,int &nN,int &nA,int &K);

/*! @brief This function builds the neighborhood of each site of the lattice.
*/
void xyz_neighborhood(char lattice[],cl_int **voisin_xyz);

/*! @brief This function maps the lattice to orthogonal vectors (it is already done by definition fotr simple cubic lattice).
*/
void lattice_mapping(char lattice[],int L,cl_int *voisin_xyz,cl_int **l_voisins,int **mapping);

/*! @brief from site "s", we get site "s-1" with "v" and site "s+1" with "w" ...
*/
void from_s_to_spm1(const char *lattice,int *l_voisins,cl_int **v_possibles,cl_int **w_possibles,cl_int **l_possibles,int **voisin_voisin);

/*! @brief load initial conformation or create one if it does not exist.
 */
void load_conformation(char name[],char lattice[],int seed,int N,int L,cl_int *voisin_xyz,int *mapping,cl_int *l_voisins,double &cum_S,cl_int **x_cum,cl_int **y_cum,cl_int **z_cum,cl_int **occupation,cl_int **i_to_site);

/*! @brief from 'i' to 'bond'
 */
void i_to_bond(char lattice[],int N,cl_int *l_voisins,cl_int *i_to_site,int **i_to_bond1,int **i_to_bond2);

/*! @brief the center-of-mass of the unwrap conformation is moved to (0,0,0)
*/
void com_to_0(int N,cl_int **x_cum,cl_int **y_cum,cl_int **z_cum);

/*! @brief save the conformation details with bond representation.
*/
void save_structure(char name[],char lattice[],int N,cl_int *x_cum,cl_int *y_cum,cl_int *z_cum);

/*! @brief save the conformation with bond representation, append to existing file.
*/
void save_conformation(char name[],char lattice[],int N,cl_int *x_cum,cl_int *y_cum,cl_int *z_cum,cl_int *i_to_site,double MCS);

/*! @brief save for visualisation with bond representation.
 */
int save_for_visualisation(char name[],const char *wa,char lattice[],int N,cl_int *x_cum,cl_int *y_cum,cl_int *z_cum);

/*! @brief get the platform information and select one. return 0 iif no platform has been found, 1 otherwize.
 */
int get_platform_information(const char *gpu,int N,std::size_t **global_work_size,std::size_t **local_work_size,cl_int &ret,cl_int &selection_device,cl_int &selection_platform,cl_device_id **devices,cl_platform_id **platforms);

/*! @brief check the bonds one site makes. return 1 if chek is success, 0 otherwize.
 */
int check_the_bonds(char lattice[],int N,int L,cl_int *i_to_site,int *voisin_xyz,int *occupation);

/*! @brief rho, voisins and possibles from lattice, polymer size and lattice size
 */
void rho_voisins_possibles(const char *lattice,int N,int L,double &rho,int &voisins,int &possibles);

/*! @brief seeds for each site
 */
void generate_seeds(int N,int seed,cl_ulong *&graine,cl_ulong *&graine0,cl_ulong *&graine1,const char *sname);

/*! @brief write seeds for each site
 */
void save_seeds(int N,const cl_ulong *graine,const cl_ulong *graine0,const cl_ulong *graine1,const char *sname);

/*! @brief load kernels from file
 */
void kernels_from_file(const char *lattice,cl_kernel kernels[3],cl_program program);

#endif
