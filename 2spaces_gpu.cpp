/*!
  \file 2spaces_gpu.cpp is distributed under MIT licence.
  \brief Main code used as an example on how to use the 2-spaces algorithm kernel defined in 'kernels.c'.
  It runs a polymer of size that is a power of 2 on a lattice that is also a power of 2.
  The implementation is OpenCL therefore the code can be used on both CPU and GPU hardware.
  \author Pascal Carrivain
  \date 07 October 2020
*/
#include "functions.h"
#include "variables.h"

int arg_i;

using namespace std;

// main
int main(int argc,char **argv){
  TInitialPC=clock();
  retour=sprintf(lattice,"%s","fcc");
  retour=sprintf(path_to,"%s","/scratch/2spaces_gpu");
  const option long_opts[]={
    {"lattice",required_argument,nullptr,'L'},
    {"seed",required_argument,nullptr,'s'},
    {"every",required_argument,nullptr,'e'},
    {"stop",required_argument,nullptr,'S'},
    {"gpu",optional_argument,nullptr,'g'}
  };
  while((arg_i=getopt_long(argc,argv,"L:s:e:r:n:l:S:m:g:w:h",long_opts,nullptr))!=-1){
    switch(arg_i){
    case 'L':
      sprintf(lattice,"%s",optarg);
      break;
    case 's':
      seed=(int)atof(optarg);
      break;
    case 'e':
      every=(int)atof(optarg);
      break;
    case 'n':
      nN=(int)atof(optarg);
      N=1<<nN;
      break;
    case 'l':
      nL=(int)atof(optarg);
      L=1<<nL;
      break;
    case 'S':
      retour=0;
      sprintf(spq,"%s",optarg);
      spq_tok=strtok(spq,".");
      while(spq_tok!=NULL && retour<3){
	// printf("%s\n",spq_tok);
	// if(retour==0) sSTOP=(int)atof(spq_tok);
	// if(retour==1) pSTOP=(int)atof(spq_tok);
	// if(retour==2) qSTOP=(int)atof(spq_tok);
	spq_tok=strtok(NULL,".");
	retour++;
      }
    case 'm':
      MCS=pow(2.,atof(optarg));
      break;
    case 'g':
      sprintf(gpu,"%s",optarg);
      break;
    case 'w':
      sprintf(path_to,"%s",optarg);
      break;
    case 'h':
      printf("2spaces_gpu.cpp usage :\n");
      printf("      The following command line runs a polymer of size N, lattice with size L, initialized with a 'seed'.\n");
      printf("      Note that the size of the polymer and the size of the lattice have to be a power of 2.\n");
      printf("      Indeed, the code in the kernel uses binary operations.\n");
      printf("      The simulations stops after a number of moves 2^m.\n");
      printf("      It runs on the GPU given by '-g' or '--gpu='.\n");
      printf("      Option -w defines the path where to write the data.\n");
      printf("      Option -e saves conformation every this many number of steps.\n");
      printf("                every=-1 is every power of 2 (default)\n");
      printf("                every=0 do not save conformations (benchmark purpose)\n");
      printf("      ./executable_name -L <lattice(cs,cc,fcc)>\n");
      printf("                        -s <seed>\n");
      printf("                        -e <save conformation every>\n");
      printf("                        -n <2^n polymer size>\n");
      printf("                        -l <2^l lattice edge size>\n");
      printf("                        -m <2^m number of moves before to stop>\n");
      printf("                        -g <gpu('GeForce GTX 980 Ti','Tesla P100-PCIE-16GB','Tesla K80','Tesla K40m','GeForce RTX 2060 SUPER'...)>\n");
      printf("                        -w <where to write the data>\n");
      printf("      ./executable_name -L fcc -s 1 -n 17 -l 6 -m 20 -g 'Tesla P100-PCIE-16GB' -w /scratch/2spaces_gpu\n");
      return 0;
      break;
    default:
      break;
    }
  }
  replace_char_a_with_b(gpu,GPU," ","_",200);
  // density, neighborhood and possible moves
  rho_voisins_possibles(lattice,N,L,rho,voisins,possibles);
  // N and A has to be a power of 2
  check_N_and_A_power_of_2(lattice,L,N,A,nN,nA,K);
  // xyz neighborhood
  xyz_neighborhood(lattice,&voisin_xyz);
  // mapping
  lattice_mapping(lattice,L,voisin_xyz,&l_voisins,&mapping);
  // from site "s", we get site "s-1" with "v" and site "s+1" with "w" ...
  from_s_to_spm1(lattice,l_voisins,&v_possibles,&w_possibles,&l_possibles,&voisin_voisin);
  // seeds
  retour=sprintf(fname,"%s/pRNGs_state_%s_n%i_rho%f_L%i_N%i_%s.out",path_to,lattice,seed,rho,L,N,GPU);
  generate_seeds(N,seed,seeds,seeds0,seeds1,fname);
  // already existing conformation ?
  retour=sprintf(fname,"%s/%s_n%i_rho%f_L%i_N%i_%s.out",path_to,lattice,seed,rho,L,N,GPU);
  load_conformation(fname,lattice,seed,N,L,voisin_xyz,mapping,l_voisins,cum_MCS,&x_cum,&y_cum,&z_cum,&occupation,&i_to_site);
  delete[] mapping;
  mapping=NULL;
  // s-1=previous(s) and next(s)=s+1
  i_to_bond(lattice,N,l_voisins,i_to_site,&i_to_bond1,&i_to_bond2);
  delete[] l_voisins;
  l_voisins=NULL;
  // com to (0,0,0)
  com_to_0(N,&x_cum,&y_cum,&z_cum);
  // save the structure
  retour=sprintf(fname,"%s/i%s_n%i_rho%f_L%i_N%i_%s.out",path_to,lattice,seed,rho,L,N,GPU);
  save_structure(fname,lattice,N,x_cum,y_cum,z_cum);

  // load kernel source file
  FILE *fp=fopen(fileName,"r");
  char *source_str=(char *)malloc(MAX_SOURCE_SIZE);
  source_size=fread(source_str,1,MAX_SOURCE_SIZE,fp);
  fclose(fp);
  // get platform information
  get_platform_information(gpu,N,&global_work_size,&local_work_size,ret,selection_device,selection_platform,&devices,&platforms);
  // create OpenCL context
  context=clCreateContext(NULL,1,&devices[selection_device],NULL,NULL,&ret);
  printf("\ncontext : %i/%i\n",ret,(int)CL_SUCCESS);
  // create command queue
  // cl_command_queue command_queue=clCreateCommandQueue(context,devices[selection_device],0,&ret);
  cl_command_queue command_queue=clCreateCommandQueue(context,devices[selection_device],CL_QUEUE_PROFILING_ENABLE,&ret);
  printf("command_queue %i/%i\n",ret,(int)CL_SUCCESS);
  // create kernel program from source file
  program=clCreateProgramWithSource(context,1,(const char **)&source_str,(const std::size_t *)&source_size,&ret);
  printf("program : %i/%i\n",ret,(int)CL_SUCCESS);
  ret=clBuildProgram(program,1,&devices[selection_device],NULL,NULL,NULL);
  printf("build : %i/%i\n",ret,(int)CL_SUCCESS);
  // determine the size of the log
  clGetProgramBuildInfo(program,devices[selection_device],CL_PROGRAM_BUILD_LOG,0,NULL,&log_size);
  // allocate memory for the log
  char *log_msg=(char *)malloc(log_size);
  // get and print the log
  clGetProgramBuildInfo(program,devices[selection_device],CL_PROGRAM_BUILD_LOG,log_size,log_msg,NULL);
  printf("%s\n",log_msg);
  // create data parallel OpenCL kernel
  kernels_from_file(lattice,kernels,program);

  // cl buffers
  cl_mem i_to_site_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_int)*N,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem occupation_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_int)*2*K,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem v_possibles_ROmobj=clCreateBuffer(context,CL_MEM_READ_ONLY,sizeof(cl_int)*voisins*voisins*possibles,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem w_possibles_ROmobj=clCreateBuffer(context,CL_MEM_READ_ONLY,sizeof(cl_int)*voisins*voisins*possibles,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem l_possibles_ROmobj=clCreateBuffer(context,CL_MEM_READ_ONLY,sizeof(cl_int)*voisins*voisins,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem voisin_voisin_ROmobj=clCreateBuffer(context,CL_MEM_READ_ONLY,sizeof(cl_int)*voisins,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem i_to_bond1_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_int)*N,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem i_to_bond2_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_int)*N,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem x_cum_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_int)*N,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem y_cum_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_int)*N,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem z_cum_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_int)*N,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem seeds_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_ulong)*N,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem seeds0_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_ulong)*N/2,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem seeds1_mobj=clCreateBuffer(context,CL_MEM_READ_WRITE,sizeof(cl_ulong)*N/2,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);
  cl_mem voisin_xyz_ROmobj=clCreateBuffer(context,CL_MEM_READ_ONLY,sizeof(cl_int)*3*voisins,NULL,&ret);
  if(ret!=0) printf("cl_mem %i\n",ret);

  // copy to the "memory buffer" for use in the kernel
  CL_CHECK(clEnqueueWriteBuffer(command_queue,i_to_bond1_mobj,CL_TRUE,0,sizeof(cl_int)*N,i_to_bond1,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,i_to_bond2_mobj,CL_TRUE,0,sizeof(cl_int)*N,i_to_bond2,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,i_to_site_mobj,CL_TRUE,0,sizeof(cl_int)*N,i_to_site,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,occupation_mobj,CL_TRUE,0,sizeof(cl_int)*2*K,occupation,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,v_possibles_ROmobj,CL_TRUE,0,sizeof(cl_int)*voisins*voisins*possibles,v_possibles,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,w_possibles_ROmobj,CL_TRUE,0,sizeof(cl_int)*voisins*voisins*possibles,w_possibles,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,l_possibles_ROmobj,CL_TRUE,0,sizeof(cl_int)*voisins*voisins,l_possibles,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,voisin_voisin_ROmobj,CL_TRUE,0,sizeof(cl_int)*voisins,voisin_voisin,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,x_cum_mobj,CL_TRUE,0,sizeof(cl_int)*N,x_cum,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,y_cum_mobj,CL_TRUE,0,sizeof(cl_int)*N,y_cum,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,z_cum_mobj,CL_TRUE,0,sizeof(cl_int)*N,z_cum,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,seeds_mobj,CL_TRUE,0,sizeof(cl_ulong)*N,seeds,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,seeds0_mobj,CL_TRUE,0,sizeof(cl_ulong)*N/2,seeds0,0,NULL,NULL));
  CL_CHECK(clEnqueueWriteBuffer(command_queue,seeds1_mobj,CL_TRUE,0,sizeof(cl_ulong)*N/2,seeds1,0,NULL,NULL));  
  CL_CHECK(clEnqueueWriteBuffer(command_queue,voisin_xyz_ROmobj,CL_TRUE,0,sizeof(cl_int)*3*voisins,voisin_xyz,0,NULL,NULL));
  CL_CHECK(clFinish(command_queue));
  // kernels : arguments
  for(int k=0;k<3;k++){
    CL_CHECK(clSetKernelArg(kernels[k],0,sizeof(i_to_site_mobj),(void *)&i_to_site_mobj));
    CL_CHECK(clSetKernelArg(kernels[k],1,sizeof(occupation_mobj),(void *)&occupation_mobj));
    CL_CHECK(clSetKernelArg(kernels[k],2,sizeof(v_possibles_ROmobj),(void *)&v_possibles_ROmobj));
    CL_CHECK(clSetKernelArg(kernels[k],3,sizeof(w_possibles_ROmobj),(void *)&w_possibles_ROmobj));
    CL_CHECK(clSetKernelArg(kernels[k],4,sizeof(l_possibles_ROmobj),(void *)&l_possibles_ROmobj));
    CL_CHECK(clSetKernelArg(kernels[k],5,sizeof(i_to_bond1_mobj),(void *)&i_to_bond1_mobj));
    CL_CHECK(clSetKernelArg(kernels[k],6,sizeof(i_to_bond2_mobj),(void *)&i_to_bond2_mobj));
    CL_CHECK(clSetKernelArg(kernels[k],7,sizeof(voisin_voisin_ROmobj),(void *)&voisin_voisin_ROmobj));
    CL_CHECK(clSetKernelArg(kernels[k],8,sizeof(x_cum_mobj),(void *)&x_cum_mobj));
    CL_CHECK(clSetKernelArg(kernels[k],9,sizeof(y_cum_mobj),(void *)&y_cum_mobj));
    CL_CHECK(clSetKernelArg(kernels[k],10,sizeof(z_cum_mobj),(void *)&z_cum_mobj));
    CL_CHECK(clSetKernelArg(kernels[k],11,sizeof(voisin_xyz_ROmobj),(void *)&voisin_xyz_ROmobj));
    if(k==0) CL_CHECK(clSetKernelArg(kernels[k],12,sizeof(seeds0_mobj),(void *)&seeds0_mobj));
    if(k==1) CL_CHECK(clSetKernelArg(kernels[k],12,sizeof(seeds1_mobj),(void *)&seeds1_mobj));
    if(k==2) CL_CHECK(clSetKernelArg(kernels[k],12,sizeof(seeds_mobj),(void *)&seeds_mobj));
    CL_CHECK(clSetKernelArg(kernels[k],13,sizeof(int),&N));
    if(k==2) CL_CHECK(clSetKernelArg(kernels[k],14,sizeof(int),&nN));
    CL_CHECK(clSetKernelArg(kernels[k],14+(k==2),sizeof(int),&K));
    CL_CHECK(clSetKernelArg(kernels[k],15+(k==2),sizeof(int),&A));
    CL_CHECK(clSetKernelArg(kernels[k],16+(k==2),sizeof(int),&nA));
  }

  // init pRNG for varaible 'space'
  std::mt19937_64 e1(seed);
  std::uniform_real_distribution<double> uniform_real_dist(0.0,1.0);
  // simulation loop
  while(pas<=MCS){
    space=(int)(uniform_real_dist(e1)<.5);
    // run the kernel
    CL_CHECK(clSetKernelArg(kernels[space],17+(space==2),sizeof(int),&space));
    CL_CHECK(clEnqueueNDRangeKernel(command_queue,kernels[space],1,NULL,global_work_size,local_work_size,0,NULL,&kernel_completion));
    CL_CHECK(clWaitForEvents(1,&kernel_completion));
    CL_CHECK(clFinish(command_queue));
    CL_CHECK(clGetEventProfilingInfo(kernel_completion,CL_PROFILING_COMMAND_START,sizeof(time_start),&time_start,NULL));
    CL_CHECK(clGetEventProfilingInfo(kernel_completion,CL_PROFILING_COMMAND_END,sizeof(time_end),&time_end,NULL));
    CL_CHECK(clReleaseEvent(kernel_completion));
    T0+=(time_end-time_start)*1e-9;// in seconds
    // conformation 2^k if every<0
    if((every<0 && ((cum_MCS+pas)==0 || modf(log2(cum_MCS+pas),&d0)==.0)) || (every>0 && modf((cum_MCS+pas)/max(1,every),&d0)==.0)){
      CL_CHECK(clEnqueueReadBuffer(command_queue,x_cum_mobj,CL_TRUE,0,sizeof(cl_int)*N,x_cum,0,NULL,NULL));
      CL_CHECK(clEnqueueReadBuffer(command_queue,y_cum_mobj,CL_TRUE,0,sizeof(cl_int)*N,y_cum,0,NULL,NULL));
      CL_CHECK(clEnqueueReadBuffer(command_queue,z_cum_mobj,CL_TRUE,0,sizeof(cl_int)*N,z_cum,0,NULL,NULL));
      CL_CHECK(clEnqueueReadBuffer(command_queue,i_to_site_mobj,CL_TRUE,0,sizeof(cl_int)*N,i_to_site,0,NULL,NULL));
      switch(space){
      case 0:
	CL_CHECK(clEnqueueReadBuffer(command_queue,seeds0_mobj,CL_TRUE,0,sizeof(cl_ulong)*N/2,seeds0,0,NULL,NULL));
	break;
      case 1:
	CL_CHECK(clEnqueueReadBuffer(command_queue,seeds1_mobj,CL_TRUE,0,sizeof(cl_ulong)*N/2,seeds1,0,NULL,NULL));
	break;
      case 2:
	CL_CHECK(clEnqueueReadBuffer(command_queue,seeds_mobj,CL_TRUE,0,sizeof(cl_ulong)*N,seeds,0,NULL,NULL));
	break;
      default:
	break;
      }
      CL_CHECK(clEnqueueReadBuffer(command_queue,occupation_mobj,CL_TRUE,0,sizeof(cl_int)*2*K,occupation,0,NULL,NULL));
      CL_CHECK(clFinish(command_queue));
      // check the bonds
      retour=check_the_bonds(lattice,N,L,i_to_site,voisin_xyz,occupation);
      // save conformation
      retour=sprintf(fname,"%s/%s_n%i_rho%f_L%i_N%i_%s.out",path_to,lattice,seed,rho,L,N,GPU);
      save_conformation(fname,lattice,N,x_cum,y_cum,z_cum,i_to_site,pas);
      // save for visualisation
      retour=sprintf(fname,"%s/blender_%s_n%i_rho%f_L%i_N%i_%s.out",path_to,lattice,seed,rho,L,N,GPU);
      save_for_visualisation(fname,"a",lattice,N,x_cum,y_cum,z_cum);
      // save pRNGs state
      retour=sprintf(fname,"%s/pRNGs_state_%s_n%i_rho%f_L%i_N%i_%s.out",path_to,lattice,seed,rho,L,N,GPU);
      save_seeds(N,seeds,seeds0,seeds1,fname);
      // print some informations
      printf("%.0f hMCS in %f seconds (kernel)\n",pas,T0);
      retour=sprintf(fname,"%s/log_%s_n%i_rho%f_L%i_N%i_%s.out",path_to,lattice,seed,rho,L,N,GPU);
      fOut=fopen(fname,"a");
      retour=fprintf(fOut,"%.0f hMCS in %f seconds (kernel)\n",pas,T0);
      fclose(fOut);
    }
    pas+=1.;
  }
  // fprintf some informations
  retour=sprintf(fname,"%s/log_%s_n%i_rho%f_L%i_N%i_%s.out",path_to,lattice,seed,rho,L,N,GPU);
  fOut=fopen(fname,"a");
  retour=fprintf(fOut,"KERNEL=%f/%f seconds\n",T0,(clock()-TInitialPC)/CLOCKS_PER_SEC);
  retour=fprintf(fOut,"particle update(per second)=%f\n",(MCS*.5*N)/T0);
  fclose(fOut);
  // clean OpenCL
  CL_CHECK(clReleaseMemObject(i_to_site_mobj));
  CL_CHECK(clReleaseMemObject(occupation_mobj));
  CL_CHECK(clReleaseMemObject(v_possibles_ROmobj));
  CL_CHECK(clReleaseMemObject(w_possibles_ROmobj));
  CL_CHECK(clReleaseMemObject(l_possibles_ROmobj));
  CL_CHECK(clReleaseMemObject(i_to_bond1_mobj));
  CL_CHECK(clReleaseMemObject(i_to_bond2_mobj));
  CL_CHECK(clReleaseMemObject(voisin_voisin_ROmobj));
  CL_CHECK(clReleaseMemObject(x_cum_mobj));
  CL_CHECK(clReleaseMemObject(y_cum_mobj));
  CL_CHECK(clReleaseMemObject(z_cum_mobj));
  CL_CHECK(clReleaseMemObject(seeds_mobj));
  CL_CHECK(clReleaseMemObject(seeds0_mobj));
  CL_CHECK(clReleaseMemObject(seeds1_mobj));
  for(int k=0;k<3;k++) CL_CHECK(clReleaseKernel(kernels[k]));
  CL_CHECK(clReleaseProgram(program));
  CL_CHECK(clReleaseContext(context));
  CL_CHECK(clReleaseDevice(devices[0]));
  // clean variables
  delete[] global_work_size;
  delete[] local_work_size;
  delete[] seeds;
  delete[] seeds0;
  delete[] seeds1;
  delete[] i_to_site;
  delete[] occupation;
  delete[] x_cum;
  delete[] y_cum;
  delete[] z_cum;
  delete[] voisin_xyz;
  delete[] v_possibles;
  delete[] w_possibles;
  delete[] l_possibles;
  delete[] voisin_voisin;
  delete[] i_to_bond1;
  delete[] i_to_bond2;
  delete[] platforms;
  delete[] devices;
  delete[] GPU;
  return 0;
}
