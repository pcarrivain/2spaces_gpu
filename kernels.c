#pragma OPENCL_EXTENSION cl_amd_printf:enable
#pragma OPENCL EXTENSION cl_khr_fp64:enable

#define inv_64 5.42101086242752217003726400434970855712890625e-20

/* ----- kernel fcc ----- */
__kernel void fcc(__global int *i_to_site,__global int *occupation,__global int *v_possibles,__global int *w_possibles,__global int *l_possibles,__global int *i_to_bond1,__global int *i_to_bond2,__global int *voisin_voisin,__global int *x_cum,__global int *y_cum,__global int *z_cum,__global int *voisin_xyz,__global ulong *seeds,int N,int nN,int K,int A,int nA,int space){
  int i=(get_global_id(0)<<1)+space;
  __local int signe;
  signe=1-(space<<1);
  if(i<N){
    __private int indice,i1,lien,voisin,xi,yi,zi,ii,jj,kk,vv,ww,VV,WW;
    __private int aa,bb,cc;
    __private bool b0,b1;
    __private ulong xx;
    vv=i_to_bond1[i];
    ww=i_to_bond2[i];
    // xorshift128 prn
    xx=seeds[i];
    xx^=xx>>12;
    xx^=xx<<25;
    xx^=xx>>27;
    xx*=0x2545f4914f6cdd1d;
    seeds[i]=xx;
    voisin=(int)((double)(12*xx)*inv_64);
    if(voisin<l_possibles[vv*12+ww]){
      VV=v_possibles[(vv*12+ww)*12+voisin];
      indice=i_to_site[i];
      /* (x,y,z) site s(i) */
      xi=(indice>>(nA+nA));
      yi=((indice>>nA)&(A-1));
      zi=(indice&(A-1));
      /* (x,y,z) site s(i-1), site s'(i) */
      aa=voisin_xyz[3*vv]+voisin_xyz[3*VV];
      bb=voisin_xyz[3*vv+1]+voisin_xyz[3*VV+1];
      cc=voisin_xyz[3*vv+2]+voisin_xyz[3*VV+2];
      ii=xi+aa;
      jj=yi+bb;
      kk=zi+cc;
      // 1-(i+A)>>nA :
      // if i=-1 then 1-(A-1)>>nA=1
      // if i=0 then 1-(A)>>nA=0
      // if i=A-1 then 1-(2*A-1)>>nA=0
      // if i=A then 1-(2*A)>>nA=-1
      ii+=(1-((ii+A)>>nA))*A;
      jj+=(1-((jj+A)>>nA))*A;
      kk+=(1-((kk+A)>>nA))*A;
      i1=A*(A*ii+jj)+kk;
      /* loop over the neighborhood of the new site */
      lien=0;
      for(int x=-1;x<=1;x+=2){
      	xi=ii+x;
      	xi+=(1-((xi+A)>>nA))*A;
      	yi=jj-1;
      	yi+=(1-((yi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+yi)+kk];
      	yi=jj+1;
      	yi+=(1-((yi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+yi)+kk];
      	zi=kk-1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+jj)+zi];
      	zi=kk+1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+jj)+zi];
      }
      for(int y=-1;y<=1;y+=2){
      	yi=jj+y;
      	yi+=(1-((yi+A)>>nA))*A;
      	zi=kk-1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*ii+yi)+zi];
      	zi=kk+1;
      	zi+=(1-((zi+A)>>nA))*A;
      	lien+=occupation[K*((1-(-signe))>>1)+A*(A*ii+yi)+zi];
      }
      b0=(i>0);
      b1=(i<(N-1));
      /* b0=(N-1+i)>>nN; */
      /* b1=1-(i+1)>>nN; */
      if(lien==(b0+b1)){
	occupation[K*((1-signe)>>1)+indice]=0;
	i_to_site[i]=i1;
	occupation[K*((1-signe)>>1)+i1]=1;
	// unwrap coordinates
	x_cum[i]+=aa;
	y_cum[i]+=bb;
	z_cum[i]+=cc;
	// compute new bonds
	WW=w_possibles[(vv*12+ww)*12+voisin];
	/* i_to_bond1[i]=voisin_voisin[VV]; */
	/* i_to_bond2[i]=voisin_voisin[WW]; */
	i_to_bond1[i]=11-VV;
	i_to_bond2[i]=11-WW;
	if(b0){
	  i_to_bond2[i-1]=VV;
	  if(i==1) i_to_bond1[0]=VV;
	}
	if(b1){
	  i_to_bond1[i+1]=WW;
	  if(i==(N-2)) i_to_bond2[N-1]=WW;
	}
      }
    }
  }
  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
}

/* ----- kernel fcc first half-space ----- */
__kernel void fcc_0(__global int *i_to_site,__global int *occupation,__global int *v_possibles,__global int *w_possibles,__global int *l_possibles,__global int *i_to_bond1,__global int *i_to_bond2,__global int *voisin_voisin,__global int *x_cum,__global int *y_cum,__global int *z_cum,__global int *voisin_xyz,__global ulong *seeds0,int N,int K,int A,int nA,int space){
  int i=(get_global_id(0)<<1)+space;
  __local int signe;
  signe=1-(space<<1);
  /* __local local_voisin_voisin[12]; */
  if(i<N){
    __private int indice,i1,lien,voisin,xi,yi,zi,ii,jj,kk,vv,ww,VV,WW;
    __private int aa,bb,cc;
    __private bool b0,b1;
    __private ulong xx;
    vv=i_to_bond1[i];
    ww=i_to_bond2[i];
    // xorshift128 prn
    xx=seeds0[get_global_id(0)];
    xx^=xx>>12;
    xx^=xx<<25;
    xx^=xx>>27;
    xx*=0x2545f4914f6cdd1d;
    seeds0[get_global_id(0)]=xx;
    voisin=(int)((double)(12)*(double)(xx)*inv_64);
    if(voisin<l_possibles[vv*12+ww]){
      VV=v_possibles[(vv*12+ww)*12+voisin];
      indice=i_to_site[i];
      /* (x,y,z) site s(i) */
      xi=(indice>>(nA+nA));
      yi=((indice>>nA)&(A-1));
      zi=(indice&(A-1));
      /* (x,y,z) site s(i-1), site s'(i) */
      aa=voisin_xyz[3*vv]+voisin_xyz[3*VV];
      bb=voisin_xyz[3*vv+1]+voisin_xyz[3*VV+1];
      cc=voisin_xyz[3*vv+2]+voisin_xyz[3*VV+2];
      ii=xi+aa;
      jj=yi+bb;
      kk=zi+cc;
      /* ii=xi+voisin_xyz[3*vv]+voisin_xyz[3*VV]; */
      /* jj=yi+voisin_xyz[3*vv+1]+voisin_xyz[3*VV+1]; */
      /* kk=zi+voisin_xyz[3*vv+2]+voisin_xyz[3*VV+2]; */
      // justification de l'expression 1-(i+A)>>nA :
      // si i=-1 alors 1-(A-1)>>nA=1
      // si i=0 alors 1-(A)>>nA=0
      // si i=A-1 alors 1-(2*A-1)>>nA=0
      // si i=A alors 1-(2*A)>>nA=-1
      ii+=(1-((ii+A)>>nA))*A;
      jj+=(1-((jj+A)>>nA))*A;
      kk+=(1-((kk+A)>>nA))*A;
      i1=A*(A*ii+jj)+kk;
      /* loop over new site neighborhood */
      lien=0;
      for(int x=-1;x<=1;x+=2){
      	xi=ii+x;
      	xi+=(1-((xi+A)>>nA))*A;
      	yi=jj-1;
      	yi+=(1-((yi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+yi)+kk];
      	yi=jj+1;
      	yi+=(1-((yi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+yi)+kk];
      	zi=kk-1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+jj)+zi];
      	zi=kk+1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+jj)+zi];
      }
      for(int y=-1;y<=1;y+=2){
      	yi=jj+y;
      	yi+=(1-((yi+A)>>nA))*A;
      	zi=kk-1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*ii+yi)+zi];
      	zi=kk+1;
      	zi+=(1-((zi+A)>>nA))*A;
      	lien+=occupation[K*((1-(-signe))>>1)+A*(A*ii+yi)+zi];
      }
      b0=(i>0);
      b1=(i<(N-1));
      // printf("%i %i\n",lien,b0+b1);
      if(lien==(b0+b1)){
	occupation[K*((1-signe)>>1)+indice]=0;
	i_to_site[i]=i1;
	occupation[K*((1-signe)>>1)+i1]=1;
	// unwrap coordinates
	x_cum[i]+=aa;
	y_cum[i]+=bb;
	z_cum[i]+=cc;
	// printf("%i %i %i\n",x_cum[i],y_cum[i],z_cum[i]);
	// compute new links
	WW=w_possibles[(vv*12+ww)*12+voisin];
	/* i_to_bond1[i]=voisin_voisin[VV]; */
	/* i_to_bond2[i]=voisin_voisin[WW]; */
	i_to_bond1[i]=11-VV;
	i_to_bond2[i]=11-WW;
	if(b0){
	  i_to_bond2[i-1]=VV;
	  if(i==1) i_to_bond1[0]=VV;
	}
	if(b1){
	  i_to_bond1[i+1]=WW;
	  if(i==(N-2)) i_to_bond2[N-1]=WW;
	}
      }
    }
  }
  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
}

/* ----- kernel fcc second half-space ----- */
__kernel void fcc_1(__global int *i_to_site,__global int *occupation,__global int *v_possibles,__global int *w_possibles,__global int *l_possibles,__global int *i_to_bond1,__global int *i_to_bond2,__global int *voisin_voisin,__global int *x_cum,__global int *y_cum,__global int *z_cum,__global int *voisin_xyz,__global ulong *seeds1,int N,int K,int A,int nA,int space){
  int i=(get_global_id(0)<<1)+space;
  __local int signe;
  signe=1-(space<<1);
  /* __local local_voisin_voisin[12]; */
  if(i<N){
    __private int indice,i1,lien,voisin,xi,yi,zi,ii,jj,kk,vv,ww,VV,WW;
    __private int aa,bb,cc;
    __private bool b0,b1;
    __private ulong xx;
    vv=i_to_bond1[i];
    ww=i_to_bond2[i];
    // xorshift128 prn
    xx=seeds1[get_global_id(0)];
    xx^=xx>>12;
    xx^=xx<<25;
    xx^=xx>>27;
    xx*=0x2545f4914f6cdd1d;
    seeds1[get_global_id(0)]=xx;
    voisin=(int)((double)(12)*(double)(xx)*inv_64);
    if(voisin<l_possibles[vv*12+ww]){
      VV=v_possibles[(vv*12+ww)*12+voisin];
      indice=i_to_site[i];
      /* (x,y,z) site s(i) */
      xi=(indice>>(nA+nA));
      yi=((indice>>nA)&(A-1));
      zi=(indice&(A-1));
      /* (x,y,z) site s(i-1) then site s'(i) */
      aa=voisin_xyz[3*vv]+voisin_xyz[3*VV];
      bb=voisin_xyz[3*vv+1]+voisin_xyz[3*VV+1];
      cc=voisin_xyz[3*vv+2]+voisin_xyz[3*VV+2];
      ii=xi+aa;
      jj=yi+bb;
      kk=zi+cc;
      /* ii=xi+voisin_xyz[3*vv]+voisin_xyz[3*VV]; */
      /* jj=yi+voisin_xyz[3*vv+1]+voisin_xyz[3*VV+1]; */
      /* kk=zi+voisin_xyz[3*vv+2]+voisin_xyz[3*VV+2]; */
      // justification de l'expression 1-(i+A)>>nA :
      // si i=-1 alors 1-(A-1)>>nA=1
      // si i=0 alors 1-(A)>>nA=0
      // si i=A-1 alors 1-(2*A-1)>>nA=0
      // si i=A alors 1-(2*A)>>nA=-1
      ii+=(1-((ii+A)>>nA))*A;
      jj+=(1-((jj+A)>>nA))*A;
      kk+=(1-((kk+A)>>nA))*A;
      i1=A*(A*ii+jj)+kk;
      /* loop over the neighborhood of the new site */
      lien=0;
      for(int x=-1;x<=1;x+=2){
      	xi=ii+x;
      	xi+=(1-((xi+A)>>nA))*A;
      	yi=jj-1;
      	yi+=(1-((yi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+yi)+kk];
      	yi=jj+1;
      	yi+=(1-((yi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+yi)+kk];
      	zi=kk-1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+jj)+zi];
      	zi=kk+1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*xi+jj)+zi];
      }
      for(int y=-1;y<=1;y+=2){
      	yi=jj+y;
      	yi+=(1-((yi+A)>>nA))*A;
      	zi=kk-1;
      	zi+=(1-((zi+A)>>nA))*A;
	lien+=occupation[K*((1-(-signe))>>1)+A*(A*ii+yi)+zi];
      	zi=kk+1;
      	zi+=(1-((zi+A)>>nA))*A;
      	lien+=occupation[K*((1-(-signe))>>1)+A*(A*ii+yi)+zi];
      }
      b0=(i>0);
      b1=(i<(N-1));
      if(lien==(b0+b1)){
	occupation[K*((1-signe)>>1)+indice]=0;
	i_to_site[i]=i1;
	occupation[K*((1-signe)>>1)+i1]=1;
	// unwrap coordinates
	x_cum[i]+=aa;
	y_cum[i]+=bb;
	z_cum[i]+=cc;
	// compute new bonds
	WW=w_possibles[(vv*12+ww)*12+voisin];
	/* i_to_bond1[i]=voisin_voisin[VV]; */
	/* i_to_bond2[i]=voisin_voisin[WW]; */
	i_to_bond1[i]=11-VV;
	i_to_bond2[i]=11-WW;
	if(b0){
	  i_to_bond2[i-1]=VV;
	  if(i==1) i_to_bond1[0]=VV;
	}
	if(b1){
	  i_to_bond1[i+1]=WW;
	  if(i==(N-2)) i_to_bond2[N-1]=WW;
	}
      }
    }
  }
  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
}
