/*!
  \file functions.cpp is distributed under MIT licence.
  \brief Source file for the 2-spaces algorithm kernel defined in 'kernels.c'.
  \author Pascal Carrivain
  \date 07 October 2020
*/
#include "functions.h"

/*! @brief replace 'a' with 'b' in 'c'
  @param c char to modify
  @param new_c new char
  @param a replace a ...
  @param b ... with b
  @param n size of c
 */
void replace_char_a_with_b(char c[],char *&new_c,const char *a,const char *b,int n){
  for(int i=0;i<n;i++) new_c[i]=(c[i]==*a)?*b:c[i];
}

/*! @brief check OpenCL error
  @param retour error code
 */
int CL_CHECK(cl_int retour){
  if(retour!=0) printf("error %i\n",retour);
  return 0;
}

/*! @brief periodic boundary condition for simple cubic lattice
  @param a position
  @param b left box
  @param c right box
 */
int CP_cs(int a,int b,int c){
  return a+((a<b)-(a>=c))*c;
}

/*! @brief periodic boundary condition for centered cubic lattice
  @param a position
  @param b left box
  @param c right box
 */
int CP_cc(int a,int b,int c){
  return a+((a<b)-(a>=c))*c;
}

/*! @brief periodic boundary condition for fcc lattice
  @param a position
  @param b left box
  @param c right box
 */
int CP_fcc(int a,int b,int c){
  return a+((a<b)-(a>=c))*c;
}

/*! @brief This function checks if N and A are power of 2 and store the power in the passed by reference arguments 'nN' and 'nA'.
  @param lattice cs, cc or fcc
  @param L lattice's edge
  @param N N
  @param A A
  @param nN N=2^nN
  @param nA A=2^nA
  @param K A^3
*/
void check_N_and_A_power_of_2(const char *lattice,int L,int N,int &A,int &nN,int &nA,int &K){
  double d0;
  A=2*L*(strcmp(lattice,"fcc")==0)+2*L*(strcmp(lattice,"cc")==0)+L*(strcmp(lattice,"cs")==0);
  nA=(int)(log2((double)A));
  if(modf(log2((double)A),&d0)!=0.){
    printf("A is not a power of 2, exit.\n");
    exit(EXIT_FAILURE);
  }
  nN=(int)(log2((double)N));
  if(modf(log2((double)N),&d0)!=0.){
    printf("N is not a power of 2, exit.\n");
    exit(EXIT_FAILURE);
  }
  K=A*A*A;
}

/*! @brief This function builds the neighborhood of each site of the lattice.
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param voisin_xyz neighborhood of each site (x,y,z on the lattice)
*/
void xyz_neighborhood(char lattice[],cl_int **voisin_xyz){
  int indice=0;
  int voisins=26*(strcmp(lattice,"cs")==0)+8*(strcmp(lattice,"cc")==0)+12*(strcmp(lattice,"fcc")==0);
  *voisin_xyz=new cl_int[3*voisins];
  for(int i=-1;i<2;i++){
    for(int j=-1;j<2;j++){
      for(int k=-1;k<2;k++){
	if(strcmp(lattice,"cs")==0 && !(i==0 && j==0 && k==0)){
	  (*voisin_xyz)[3*indice]=i;
	  (*voisin_xyz)[3*indice+1]=j;
	  (*voisin_xyz)[3*indice+2]=k;
	  indice++;
	}
	if(strcmp(lattice,"cc")==0 && i!=0 && j!=0 && k!=0){
	  (*voisin_xyz)[3*indice]=i;
	  (*voisin_xyz)[3*indice+1]=j;
	  (*voisin_xyz)[3*indice+2]=k;
	  indice++;
	}
	if(strcmp(lattice,"fcc")==0 && ((i==0 && j!=0 && k!=0) || (i!=0 && j==0 && k!=0) || (i!=0 && j!=0 && k==0))){
	  (*voisin_xyz)[3*indice]=i;
	  (*voisin_xyz)[3*indice+1]=j;
	  (*voisin_xyz)[3*indice+2]=k;
	  indice++;
	}
      }
    }
  }
}

/*! @brief This function maps the lattice to orthogonal vectors (it is already done by definition fotr simple cubic lattice).
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param L size of the lattice
  @param voisin_xyz neighborhood of each site (x,y,z on the lattice)
  @param l_voisins neighborhood of eahc site (index site on the lattice)
  @param mapping the mapping is passed by reference
*/
void lattice_mapping(char lattice[],int L,cl_int *voisin_xyz,cl_int **l_voisins,int **mapping){
  int ii,jj,kk,index;
  int A=L*(strcmp(lattice,"cs")==0)+2*L*(strcmp(lattice,"cc")==0)+2*L*(strcmp(lattice,"fcc")==0);
  int K=A*A*A;
  int K_lattice=(1*(strcmp(lattice,"cs")==0)+2*(strcmp(lattice,"cc")==0)+4*(strcmp(lattice,"fcc")==0))*L*L*L;
  int voisins=26*(strcmp(lattice,"cs")==0)+8*(strcmp(lattice,"cc")==0)+12*(strcmp(lattice,"fcc")==0);
  *mapping=new int[K_lattice]();
  *l_voisins=new cl_int[voisins*K]();
  // No mapping for CS
  if(strcmp(lattice,"cs")==0){
    for(int k=0;k<K_lattice;k++){
      ii=k/(A*A);
      jj=(k/A)%A;
      kk=k%A;
      for(int v=0;v<voisins;v++) (*l_voisins)[voisins*k+v]=L*L*CP_cs(ii+voisin_xyz[3*v],0,L)+L*CP_cs(jj+voisin_xyz[3*v+1],0,L)+CP_cs(kk+voisin_xyz[3*v+2],0,L);
      (*mapping)[k]=k;
    }
  }
  int i0=0;
  // Mapping : CC to orthogonal vectors
  // (i * (x + y - z) + j * (x - y + z) + k * (-x + y + z)) / 2 = (I * x + J * y + K * z) / 2
  // I = i + j - k
  // J = i - j + k
  // K = j + k - i
  // A * A * I + A * J + K with A = 2 * L
  // CC unit cell duplication
  if(strcmp(lattice,"cc")==0){
    int *CC_xyz=new int[2*3]();
    CC_xyz[3]=1;CC_xyz[4]=1;CC_xyz[5]=1;
    for(int i=0;i<L;i++){
      for(int j=0;j<L;j++){
	for(int k=0;k<L;k++){
	  for(int v=0;v<2;v++){
	    ii=2*i+CC_xyz[3*v];
	    jj=2*j+CC_xyz[3*v+1];
	    kk=2*k+CC_xyz[3*v+2];
	    index=A*A*ii+A*jj+kk;
	    for(int w=0;w<voisins;w++) (*l_voisins)[voisins*index+w]=A*A*CP_cc(ii+voisin_xyz[3*w],0,A)+A*CP_cc(jj+voisin_xyz[3*w+1],0,A)+CP_cc(kk+voisin_xyz[3*w+2],0,A);
	    (*mapping)[i0]=index;
	    i0++;
	  }
	}
      }
    }
    delete[] CC_xyz;
  }
  // Mapping : FCC to orthogonal vectors
  // (i * (x + y) + j * (x + z) + k * (y + z)) / 2 = (I * x + J * y + K * z) / 2
  // I = i + j
  // J = i + k
  // K = j + k
  // A * A * I + A * J + K with A = 2 * L
  if(strcmp(lattice,"fcc")==0){
    // FCC unit cell duplication
    int *FCC_xyz=new int[4*3]();
    FCC_xyz[3]=1;FCC_xyz[4]=1;FCC_xyz[5]=0;
    FCC_xyz[6]=1;FCC_xyz[7]=0;FCC_xyz[8]=1;
    FCC_xyz[9]=0;FCC_xyz[10]=1;FCC_xyz[11]=1;
    for(int i=0;i<L;i++){
      for(int j=0;j<L;j++){
	for(int k=0;k<L;k++){
	  for(int v=0;v<4;v++){
	    ii=2*i+FCC_xyz[3*v];
	    jj=2*j+FCC_xyz[3*v+1];
	    kk=2*k+FCC_xyz[3*v+2];
	    index=A*A*ii+A*jj+kk;
	    for(int w=0;w<voisins;w++) (*l_voisins)[voisins*index+w]=A*A*CP_fcc(ii+voisin_xyz[3*w],0,A)+A*CP_fcc(jj+voisin_xyz[3*w+1],0,A)+CP_fcc(kk+voisin_xyz[3*w+2],0,A);
	    (*mapping)[i0]=index;
	    i0++;
	  }
	}
      }
    }
    delete[] FCC_xyz;
  }
}

/*! @brief from site "s", we get site "s-1" with "v" and site "s+1" with "w" ...
  @param lattice cs, fcc or cc
  @param l_voisins for each site list of neighbors
  @param v_possibles site(v) = site-1
  @param w_possibles site(w) = site+1
  @param l_possibles list of possible moves
  @param voisin_voisin site(v)=site-1 what is 'w' such that site-1(w)=site
*/
void from_s_to_spm1(const char *lattice,int *l_voisins,cl_int **v_possibles,cl_int **w_possibles,cl_int **l_possibles,int **voisin_voisin){
  int voisins=26*(strcmp(lattice,"cs")==0)+12*(strcmp(lattice,"fcc")==0)+8*(strcmp(lattice,"cc")==0);
  int possibles=26*(strcmp(lattice,"cs")==0)+12*(strcmp(lattice,"fcc")==0)+8*(strcmp(lattice,"cc")==0);
  int index=0,i1,i2,i3;
  *v_possibles=new cl_int[voisins*voisins*possibles]();
  *w_possibles=new cl_int[voisins*voisins*possibles]();
  *l_possibles=new cl_int[voisins*voisins]();
  *voisin_voisin=new int[voisins];
  // nested loop over the neighborhood
  for(int v=0;v<voisins;v++){
    i1=l_voisins[voisins*index+v];
    for(int w=0;w<voisins;w++){
      i2=l_voisins[voisins*index+w];
      if(v==w){
	for(int i=0;i<voisins;i++){
	  (*v_possibles)[(v*voisins+w)*possibles+(*l_possibles)[v*voisins+w]]=i;
	  (*w_possibles)[(v*voisins+w)*possibles+(*l_possibles)[v*voisins+w]]=i;
	  (*l_possibles)[v*voisins+w]++;
	}
      }else{
	// Do "v" and "w" share lattice sites ?
	for(int i=0;i<voisins;i++){
	  i3=l_voisins[voisins*i1+i];
	  for(int j=0;j<voisins;j++){
	    if(i3==l_voisins[voisins*i2+j]){
	      (*v_possibles)[(v*voisins+w)*possibles+(*l_possibles)[v*voisins+w]]=i;
	      (*w_possibles)[(v*voisins+w)*possibles+(*l_possibles)[v*voisins+w]]=j;
	      (*l_possibles)[v*voisins+w]++;
	    }
	  }
	}
      }
    }
  }
  // "site s"(v)="site s-1" what is "w" such that "site s-1"(w)="site s" ?
  index=0;
  for(int v=0;v<voisins;v++){
    i1=l_voisins[voisins*index+v];
    for(int w=0;w<voisins;w++){
      if(l_voisins[voisins*i1+w]==index){
	(*voisin_voisin)[v]=w;
	// printf("%i with %i\n",v,w);
      }
    }
  }
}

/*! @brief load initial conformation or create one if it does not exist.
  @param name name of the file to the initial conformation
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param seed seed to create initial conformation
  @param N number of occupied sites
  @param L size of the lattice
  @param voisin_xyz neighborhood of each site (x,y,z on the lattice)
  @param mapping mapping
  @param l_voisins for each site list of neighbors
  @param cum_S number of steps
  @param x_cum unwrap x-coordinate
  @param y_cum unwrap y-coordinate
  @param z_cum unwrap z-coordinate
  @param occupation occupation of the sites
  @param i_to_site from polymer unit 'i' to the occupied site
 */
void load_conformation(char name[],char lattice[],int seed,int N,int L,cl_int *voisin_xyz,int *mapping,cl_int *l_voisins,double &cum_S,cl_int **x_cum,cl_int **y_cum,cl_int **z_cum,cl_int **occupation,cl_int **i_to_site){
  int voisins=26*(strcmp(lattice,"cs")==0)+12*(strcmp(lattice,"fcc")==0)+8*(strcmp(lattice,"cc")==0);
  int K_lattice=(1*(strcmp(lattice,"cs")==0)+2*(strcmp(lattice,"cc")==0)+4*(strcmp(lattice,"fcc")==0))*L*L*L;
  int A=L*(strcmp(lattice,"cs")==0)+2*L*(strcmp(lattice,"cc")==0)+2*L*(strcmp(lattice,"fcc")==0);
  int K=A*A*A;
  int ii=0,i0,i1,i2,i3,index,signe,bond,essai;
  *i_to_site=new cl_int[N];
  *occupation=new cl_int[2*K]();
  *x_cum=new cl_int[N]();
  *y_cum=new cl_int[N]();
  *z_cum=new cl_int[N]();
  FILE *fConformation=fopen(name,"r");
  if(fConformation==NULL){
    std::mt19937_64 e1(seed);
    std::uniform_real_distribution<double> uniform_real_dist(0.0,1.0);
    // initial conformation
    while(ii<N){
      signe=1-2*(ii%2);
      if(ii==0){
	index=mapping[(int)(K_lattice*uniform_real_dist(e1))];
	(*occupation)[K*(1-signe)/2+index]=1;
	(*i_to_site)[ii]=index;
	// unwrap coordinates
	(*x_cum)[0]=index/(A*A);
	(*y_cum)[1]=(index/A)%A;
	(*z_cum)[2]=index%A;
	ii++;
      }else{
	// only one bond that is between "i-1" and "i"
	bond=0;
	essai=0;
	i0=voisins*(*i_to_site)[ii-1];
	while(bond!=1 && essai<voisins){
	  i1=(int)(voisins*uniform_real_dist(e1));
	  while(voisin_xyz[3*i1+2]<0 || (*occupation)[K*(1-signe)/2+l_voisins[i0+i1]]==1) i1=(int)(voisins*uniform_real_dist(e1));
	  index=l_voisins[i0+i1];
	  bond=0;
	  for(int v=0;v<voisins;v++) bond+=(*occupation)[K*(1-(-signe))/2+l_voisins[voisins*index+v]];
	  essai++;
	}
	if(bond==1){
	  (*occupation)[K*(1-signe)/2+index]=1;
	  (*i_to_site)[ii]=index;
	  // unwrap coordinates
	  (*x_cum)[ii]=(*x_cum)[ii-1]+voisin_xyz[3*i1];
	  (*y_cum)[ii]=(*y_cum)[ii-1]+voisin_xyz[3*i1+1];
	  (*z_cum)[ii]=(*z_cum)[ii-1]+voisin_xyz[3*i1+2];
	  ii++;
	}else{
	  // go back
	  for(int i=0;i<voisins;i++){
	    ii-=(ii>0);
	    (*occupation)[K*(ii%2)+(*i_to_site)[ii]]=0;
	  }
	}
      }
    }// end (initial conformation)
    cum_S=0.;
  }else{
    // initial conformation found
    fscanf(fConformation,"%lf\n",&cum_S);
    for(int i=0;i<N;i++){
      fscanf(fConformation,"%i %i %i %i\n",&i0,&i1,&i2,&i3);
      (*x_cum)[i]=i0;
      (*y_cum)[i]=i1;
      (*z_cum)[i]=i2;
      (*occupation)[K*(i%2)+i3]=1;
      (*i_to_site)[i]=i3;
    }
    fclose(fConformation);
  }
}

/*! @brief from 'i' to previous/next bond
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param N number of occupied sites
  @param l_voisins for each site list of neighbors
  @param i_to_site from monomer to site
  @param i_to_bond1 from monomer to left bond
  @param i_to_bond2 from monomer to right bond
 */
void i_to_bond(char lattice[],int N,cl_int *l_voisins,cl_int *i_to_site,int **i_to_bond1,int **i_to_bond2){
  int index;
  int voisins=26*(strcmp(lattice,"cs")==0)+12*(strcmp(lattice,"fcc")==0)+8*(strcmp(lattice,"cc")==0);
  *i_to_bond1=new int[N]();
  *i_to_bond2=new int[N]();
  for(int i=0;i<N;i++){
    index=i_to_site[i];
    for(int v=0;v<voisins;v++){
      if(l_voisins[voisins*index+v]==i_to_site[i-(i>0)]) (*i_to_bond1)[i]=v;
      if(l_voisins[voisins*index+v]==i_to_site[i+(i<(N-1))]) (*i_to_bond2)[i]=v;
    }
  }
  (*i_to_bond1)[0]=(*i_to_bond2)[0];
  (*i_to_bond2)[N-1]=(*i_to_bond1)[N-1];
}

/*! @brief the center-of-mass of the unwrap conformation is moved to (0,0,0)
  @param N number of occupied sites
  @param x_cum unwrap x coordinate
  @param y_cum unwrap y coordinate
  @param z_cum unwrap z coordinate
*/
void com_to_0(int N,cl_int **x_cum,cl_int **y_cum,cl_int **z_cum){
  int ii=(*x_cum)[0];
  int jj=(*y_cum)[0];
  int kk=(*z_cum)[0];
  for(int i=1;i<N;i++){
    ii+=(*x_cum)[i];
    jj+=(*y_cum)[i];
    kk+=(*z_cum)[i];
  }
  for(int i=0;i<N;i++){
    (*x_cum)[i]-=(int)(ii/(double)N);
    (*y_cum)[i]-=(int)(jj/(double)N);
    (*z_cum)[i]-=(int)(kk/(double)N);
  }
}

/*! @brief save the conformation details with bond representation.
  @param name name of the file to save
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param N number of occupied sites
  @param x_cum unwrap x-coordinate
  @param y_cum unwrap y-coordinate
  @param z_cum unwrap z-coordinate
*/
void save_structure(char name[],char lattice[],int N,cl_int *x_cum,cl_int *y_cum,cl_int *z_cum){
  int ii,jj,kk,xx,yy,zz;
  double d0,T=1.*(strcmp(lattice,"cs")==0)+.5*(strcmp(lattice,"cc")==0)+.5*(strcmp(lattice,"fcc")==0);
  FILE *fSave=fopen(name,"w");
  for(int i=0;i<(N-1);i++){
    ii=x_cum[i];
    jj=y_cum[i];
    kk=z_cum[i];
    xx=x_cum[i+1];
    yy=y_cum[i+1];
    zz=z_cum[i+1];
    d0=T*sqrt((xx-ii)*(xx-ii)+(yy-jj)*(yy-jj)+(zz-kk)*(zz-kk));
    fprintf(fSave,"%i %f %f %i %i\n",0,.2*d0,d0,i,i%2);
  }
  fclose(fSave);
}

/*! @brief save the conformation with bond representation, append to existing file.
  first entry is the number of Monte-Carlo steps.
  second line is the xyz of the site 1, second line is the xyz of the site 2 ...
  @param name name of the file to save
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param N number of occupied sites
  @param x_cum unwrap x-coordinate
  @param y_cum unwrap y-coordinate
  @param z_cum unwrap z-coordinate
  @param i_to_site from polymer unit 'i' to the occupied site
  @param MCS number of Monte-Carlo steps
*/
void save_conformation(char name[],char lattice[],int N,cl_int *x_cum,cl_int *y_cum,cl_int *z_cum,cl_int *i_to_site,double MCS){
  double T=1.*(strcmp(lattice,"cs")==0)+.5*(strcmp(lattice,"cc")==0)+.5*(strcmp(lattice,"fcc")==0);
  FILE *fSave=fopen(name,"a");
  fprintf(fSave,"%.0f\n",MCS);
  for(int i=0;i<N;i++) fprintf(fSave,"%f %f %f %i\n",T*x_cum[i],T*y_cum[i],T*z_cum[i],i_to_site[i]);
  fclose(fSave);
}

/*! @brief save for visualisation with bond representation.
  first line is the com of the bond 1, second line is tangent to the bond 1 ...
  @param name name of the file to save
  @param wa write or append to the file 'name'
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param N number of occupied sites
  @param x_cum unwrap x-coordinate
  @param y_cum unwrap y-coordinate
  @param z_cum unwrap z-coordinate
*/
int save_for_visualisation(char name[],const char *wa,char lattice[],int N,cl_int *x_cum,cl_int *y_cum,cl_int *z_cum){
  int ii,jj,kk,xx,yy,zz;
  double d0,T=1.*(strcmp(lattice,"cs")==0)+.5*(strcmp(lattice,"cc")==0)+.5*(strcmp(lattice,"fcc")==0);
  FILE *fSave=fopen(name,wa);
  for(int i=0;i<(N-1);i++){
    ii=x_cum[i];
    jj=y_cum[i];
    kk=z_cum[i];
    xx=x_cum[i+1];
    yy=y_cum[i+1];
    zz=z_cum[i+1];
    if(((xx-ii)*(xx-ii)+(yy-jj)*(yy-jj)+(zz-kk)*(zz-kk))!=2){
      printf("warning bond length\n");
      return 0;
    }
    fprintf(fSave,"%f %f %f\n",.5*T*(ii+xx),.5*T*(jj+yy),.5*T*(kk+zz));
    d0=T*sqrt((xx-ii)*(xx-ii)+(yy-jj)*(yy-jj)+(zz-kk)*(zz-kk));
    fprintf(fSave,"%f %f %f\n",T*(xx-ii)/d0,T*(yy-jj)/d0,T*(zz-kk)/d0);
  }
  fclose(fSave);
  return 1;
}

/*! @brief get the platform information and select one. return 0 iif no platform has been found, 1 otherwize.
  @param gpu name of the GPU you want to use
  @param N number of polymer sites
  @param global_work_size for more details see OpenCL documentation
  @param local_work_size for more details see OpenCL documentation
  @param ret for more details see OpenCL documentation
  @param selection_device for more details see OpenCL documentation
  @param selection_platform for more details see OpenCL documentation
  @param devices for more details see OpenCL documentation
  @param platforms for more details see OpenCL documentation
 */
int get_platform_information(const char *gpu,int N,std::size_t **global_work_size,std::size_t **local_work_size,cl_int &ret,cl_int &selection_device,cl_int &selection_platform,cl_device_id **devices,cl_platform_id **platforms){
  int n_devices=10;
  cl_uint retour_platforms,retour_devices;
  cl_ulong retour_memory;
  // cl_ulong local_memory=0;
  ret=clGetPlatformIDs(10,*platforms,&retour_platforms);
  char information[1024];
  // cl_int unites=0;
  cl_int cl_i0;
  cl_bool cl_b0;
  for(int t=0;t<2;t++){
    for(unsigned int i=0;i<retour_platforms;i++){
      printf("----- platform %i/%i\n",i+1,retour_platforms);
      clGetPlatformInfo((*platforms)[i],CL_PLATFORM_PROFILE,sizeof(information)/sizeof(char),information,NULL);
      printf("%s\n",information);
      clGetPlatformInfo((*platforms)[i],CL_PLATFORM_VERSION,sizeof(information)/sizeof(char),information,NULL);
      printf("%s\n",information);
      clGetPlatformInfo((*platforms)[i],CL_PLATFORM_NAME,sizeof(information)/sizeof(char),information,NULL);
      printf("%s\n",information);
      clGetPlatformInfo((*platforms)[i],CL_PLATFORM_VENDOR,sizeof(information)/sizeof(char),information,NULL);
      printf("%s\n",information);
      clGetPlatformInfo((*platforms)[i],CL_PLATFORM_EXTENSIONS,sizeof(information)/sizeof(char),information,NULL);
      printf("%s\n",information);
      // Get device information
      if(t==0) ret=clGetDeviceIDs((*platforms)[i],CL_DEVICE_TYPE_GPU,n_devices,*devices,&retour_devices);
      if(t==1) ret=clGetDeviceIDs((*platforms)[i],CL_DEVICE_TYPE_CPU,n_devices,*devices,&retour_devices);
      if(ret!=-1){
	for(unsigned int j=0;j<retour_devices;j++){
	  printf("----- device %i/%i\n",j+1,retour_devices);
	  clGetDeviceInfo((*devices)[j],CL_DEVICE_NAME,sizeof(information)/sizeof(char),information,NULL);
	  if(strcmp(information,gpu)==0){
	    printf("%s used\n",information);
	    selection_platform=i;
	    selection_device=j;
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_AVAILABLE,sizeof(cl_bool),&cl_b0,NULL);
	    printf("available:%i/%i\n",(int)cl_b0,(int)CL_TRUE);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_ADDRESS_BITS,sizeof(cl_int),&cl_i0,NULL);
	    printf("%i bits\n",cl_i0);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_MAX_COMPUTE_UNITS,sizeof(cl_int),&cl_i0,NULL);
	    printf("CL_DEVICE_MAX_COMPUTE_UNITS %i\n",cl_i0);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_MAX_WORK_GROUP_SIZE,sizeof(cl_int),&cl_i0,NULL);
	    printf("CL_DEVICE_MAX_WORK_GROUP_SIZE : %i\n",cl_i0);
	    clGetDeviceInfo((*devices)[j],CL_KERNEL_WORK_GROUP_SIZE,sizeof(cl_int),&cl_i0,NULL);
	    printf("CL_KERNEL_WORK_GROUP_SIZE : %i\n",cl_i0);
	    clGetDeviceInfo((*devices)[j],CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,sizeof(cl_int),&cl_i0,NULL);
	    printf("CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE : %i\n",cl_i0);
	    // unites=cl_i0;
	    (*global_work_size)[0]=((N/2)/(int)cl_i0+1)*(int)cl_i0;//((N/2)/(int)unites+1)*(int)unites;
	    (*local_work_size)[0]=(int)cl_i0;//(int)unites;
	    printf("%i %i/%i\n",(int)((*local_work_size)[0]),(int)((*global_work_size)[0]),N/2);
	    clGetDeviceInfo((*devices)[j],CL_KERNEL_LOCAL_MEM_SIZE,sizeof(cl_ulong),&retour_memory,NULL);
	    printf("CL_KERNEL_LOCAL_MEM_SIZE : %li\n",retour_memory);
	    clGetDeviceInfo((*devices)[j],CL_KERNEL_PRIVATE_MEM_SIZE,sizeof(cl_ulong),&retour_memory,NULL);
	    printf("CL_KERNEL_PRIVATE_MEM_SIZE : %li\n",retour_memory);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_MAX_WORK_ITEM_SIZES,sizeof(cl_int),&cl_i0,NULL);
	    printf("CL_DEVICE_MAX_WORK_ITEM_SIZES : %i\n",cl_i0);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_VENDOR,sizeof(information)/sizeof(char),information,NULL);
	    printf("%s\n",information);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_EXTENSIONS,sizeof(information)/sizeof(char),information,NULL);
	    printf("%s\n",information);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_LOCAL_MEM_SIZE,sizeof(cl_ulong),&retour_memory,NULL);
	    printf("%li local memory size\n",retour_memory);
	    // local_memory=retour_memory;
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,sizeof(cl_ulong),&retour_memory,NULL);
	    printf("%li global cache memory size\n",retour_memory);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,sizeof(cl_ulong),&retour_memory,NULL);
	    printf("%li global cacheline memory size\n",retour_memory);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_GLOBAL_MEM_SIZE,sizeof(cl_ulong),&retour_memory,NULL);
	    printf("%li global memory size\n",retour_memory);
	    clGetDeviceInfo((*devices)[j],CL_DEVICE_VERSION,sizeof(information)/sizeof(char),information,NULL);
	    printf("%s\n",information);
	  }else printf("%s not used\n",information);
	}
      }
    }
  }
  // Get the selected device
  if(selection_platform==-1 || selection_device==-1) return 0;
  ret=clGetDeviceIDs((*platforms)[selection_platform],CL_DEVICE_TYPE_GPU,1,*devices,&retour_devices);
  return 1;
}

/*! @brief check the bonds one site makes. return 1 if chek is success, 0 otherwize.
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param N number of occupied sites.
  @param L size of the box
  @param i_to_site from the bead to the site
  @param voisin_xyz neighborhood of each site (x,y,z on the lattice)
  @param occupation occupation of the sites
 */
int check_the_bonds(char lattice[],int N,int L,cl_int *i_to_site,int *voisin_xyz,int *occupation){
  int voisins=26*(strcmp(lattice,"cs")==0)+12*(strcmp(lattice,"fcc")==0)+8*(strcmp(lattice,"cc")==0);
  int A=L*(strcmp(lattice,"cs")==0)+2*L*(strcmp(lattice,"cc")==0)+2*L*(strcmp(lattice,"fcc")==0);
  int K=A*A*A;
  int bond,i0,ii,jj,kk,index,sign_of_i;
  for(int i=0;i<N;i++){
    index=i_to_site[i];
    ii=index/(A*A);
    jj=(index/A)%A;
    kk=index%A;
    bond=0;
    sign_of_i=1-2*(i%2);
    for(int v=0;v<voisins;v++){
      i0=A*A*CP_fcc(ii+voisin_xyz[3*v],0,A)+A*CP_fcc(jj+voisin_xyz[3*v+1],0,A)+CP_fcc(kk+voisin_xyz[3*v+2],0,A);
      bond+=occupation[K*(1-(-sign_of_i))/2+i0];
    }
    if(bond!=((i>0)+(i<(N-1)))) return 0;
  }
  return 1;
}

/*! @brief rho, voisins and possibles from lattice, polymer size and lattice site
  @param lattice cs, cc or fcc
  @param N polymer size
  @param L lattice edge
  @param rho N/L^3
  @param voisins number of neighbors
  @param possibles number of possible moves
 */
void rho_voisins_possibles(const char *lattice,int N,int L,double &rho,int &voisins,int &possibles){
  if(strcmp(lattice,"cs")==0) rho=(double)N/(double)(L*L*L);
  if(strcmp(lattice,"cc")==0) rho=(double)N/(double)(L*L*L);
  if(strcmp(lattice,"fcc")==0) rho=(double)N/(double)(L*L*L);
  voisins=26*(strcmp(lattice,"cs")==0)+12*(strcmp(lattice,"fcc")==0)+8*(strcmp(lattice,"cc")==0);
  possibles=26*(strcmp(lattice,"cs")==0)+12*(strcmp(lattice,"fcc")==0)+8*(strcmp(lattice,"cc")==0);
}

/*! @brief seeds for each site
  @param N polymer size
  @param seed seed for the pRNGs
  @param graine array of seeds
  @param graine0 array of seeds for the first half-space
  @param graine1 array of seeds for the second half-space
  @param sname file name where to find seeds
 */
void generate_seeds(int N,int seed,cl_ulong *&graine,cl_ulong *&graine0,cl_ulong *&graine1,const char *sname){
  delete[] graine;
  graine=new cl_ulong[N]();
  delete[] graine0;
  graine0=new cl_ulong[N/2]();
  delete[] graine1;
  graine1=new cl_ulong[N/2]();
  std::mt19937_64 e1(seed);
  std::uniform_real_distribution<double> uniform_real_dist(0.0,1.0);
  int S=0;
  FILE *fIn=fopen(sname,"r");
  if(strcmp(sname,"")==0 || fIn==NULL){
    // no input file
    for(int i=0;i<N;i++){
      while(graine[i]==0) graine[i]=(cl_ulong)((double)(entier64)*uniform_real_dist(e1));
      if(i%2==0) graine0[i/2]=graine[i];
      else graine1[(i-1)/2]=graine[i];
    }
  }else{
    if(fIn!=NULL){
      while(!feof(fIn)){
	if(S<N) fscanf(fIn,"%lu\n",&graine[S]);
	if(S>=N && S<(N+N/2)) fscanf(fIn,"%lu\n",&graine0[S-N]);
	if(S>=(N+N/2) && S<(N+N)) fscanf(fIn,"%lu\n",&graine1[S-N-N/2]);
	S++;
      }
      fclose(fIn);
    }
    if(S!=(2*N)){
      printf("expect %i seeds, got %i, exit\n",2*N,S);
      exit(EXIT_FAILURE);
    }
  }
}

/*! @brief write seeds for each site
  @param N polymer size
  @param graine array of seeds
  @param graine0 array of seeds for the first half-space
  @param graine1 array of seeds for the second half-space
  @param sname output file name
 */
void save_seeds(int N,const cl_ulong *graine,const cl_ulong *graine0,const cl_ulong *graine1,const char *sname){
  FILE *fOut=fopen(sname,"w");
  for(int i=0;i<N;i++) fprintf(fOut,"%lu\n",graine[i]);
  for(int i=0;i<(N/2);i++) fprintf(fOut,"%lu\n",graine0[i]);
  for(int i=0;i<(N/2);i++) fprintf(fOut,"%lu\n",graine1[i]);
  fclose(fOut);
}

/*! @brief load kernels from file
  @param lattice the name of the lattice 'cs', 'cc' or 'fcc'
  @param kernels kernel according to the lattice choice
  @param program cl_program from OpenCL
 */
void kernels_from_file(const char *lattice,cl_kernel kernels[3],cl_program program){
  cl_int ret;
  kernels[0]=NULL;
  if(strcmp(lattice,"cs")==0) kernels[0]=clCreateKernel(program,"cs",&ret);
  if(strcmp(lattice,"cc")==0) kernels[0]=clCreateKernel(program,"cc",&ret);
  if(strcmp(lattice,"fcc")==0){
    kernels[2]=clCreateKernel(program,"fcc",&ret);
    kernels[0]=clCreateKernel(program,"fcc_0",&ret);
    kernels[1]=clCreateKernel(program,"fcc_1",&ret);
  }
}
